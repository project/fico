<?php

namespace Drupal\fico\Plugin\Field\FieldFormatter\Condition;

use Drupal\fico\Plugin\FieldFormatterConditionBase;
use Drupal\user\Entity\User;

/**
 * The plugin for check empty fields.
 *
 * @FieldFormatterCondition(
 *   id = "hide_not_author",
 *   label = @Translation("Hide if content is not from author"),
 *   dsFields = TRUE,
 *   types = {
 *     "all"
 *   }
 * )
 */
class HideNotAuthor extends FieldFormatterConditionBase {

  /**
   * {@inheritdoc}
   */
  public function alterForm(&$form, $settings) {
    if (isset($settings['settings']['author'])) {
      $user = User::load($settings['settings']['author']);
    }
    else {
      $user = NULL;
    }
    $config = \Drupal::config('user.settings');
    $form['author'] = [
      '#title' => t('Author'),
      '#type' => 'entity_autocomplete',
      '#target_type' => 'user',
      '#selection_settings' => ['include_anonymous' => FALSE],
      '#description' => t('Leave blank for %anonymous.', ['%anonymous' => $config->get('anonymous')]),
      '#default_value' => $user,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function access(&$build, $field, $settings) {
    $entity = $this->getEntity($build);
    if (!$entity) {
      $build[$field]['#access'] = FALSE;
      return;
    }
    if ((!$settings['settings']['author'] && $entity->getOwnerId() != 0) || ($entity->getOwnerId() != $settings['settings']['author'])) {
      $build[$field]['#access'] = FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function summary($settings) {
    if (isset($settings['settings']['author'])) {
      $user = User::load($settings['settings']['author']);
      $user = $user->getDisplayName();
    }
    else {
      $config = \Drupal::config('user.settings');
      $user = $config->get('anonymous');
    }
    return t("Condition: %condition %author", [
      "%condition" => t('Hide if content is not from author'),
      '%author' => $user,
    ]);
  }

}

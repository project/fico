<?php

namespace Drupal\fico\Plugin\Field\FieldFormatter\Condition;

use Drupal\fico\Plugin\FieldFormatterConditionBase;

/**
 * The plugin for check empty fields.
 *
 * @FieldFormatterCondition(
 *   id = "hide_date_time",
 *   label = @Translation("Hide date/time"),
 *   dsFields = TRUE,
 *   types = {
 *     "datetime",
 *     "date",
 *     "datestamp"
 *   }
 * )
 */
class HideDateTime extends FieldFormatterConditionBase {

  /**
   * {@inheritdoc}
   */
  public function alterForm(&$form, $settings) {
    $default_orientation = isset($settings['settings']['orientation']) ? $settings['settings']['orientation'] : NULL;
    $default_cutom_date = isset($settings['settings']['cutom_date']) ? $settings['settings']['cutom_date'] : NULL;
    $time_span = isset($settings['settings']['time_span']) ? $settings['settings']['time_span'] : NULL;
    $form['orientation'] = [
      '#title' => t('Hide if'),
      '#type' => 'radios',
      '#options' => [
        'small' => t("smaller than today's date"),
        'great' => t("greater than today's date"),
        'custom_small' => t("smaller then custom date"),
        'custom_great' => t("greater then custom date"),
        'older_than' => t('older than (time span)'),
        'newer_than' => t('newer than (time span)'),
      ],
      '#default_value' => $default_orientation,
    ];
    $form['cutom_date'] = [
      '#title' => t('Cutom date'),
      '#type' => 'date',
      '#default_value' => $default_cutom_date,
    ];
    $form['time_span'] = [
      '#title' => t('Time span'),
      '#type' => 'textfield',
      '#size' => 4,
      '#description' => t('Time span in days'),
      '#default_value' => $time_span,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function access(&$build, $field, $settings) {
    $custom_date = strtotime($settings['settings']['cutom_date']);
    if (!empty($build[$field]['#items'])) {
      foreach ($build[$field]['#items'] as $item) {
        $info = $item->getValue($field);
        switch ($settings['settings']['orientation']) {
          case 'small':
            if (strtotime($info['value']) < \Drupal::time()->getRequestTime()) {
              $build[$field]['#access'] = FALSE;
            }
            break;

          case 'great':
            if (strtotime($info['value']) > \Drupal::time()->getRequestTime()) {
              $build[$field]['#access'] = FALSE;
            }
            break;

          case 'custom_small':
            if (strtotime($info['value']) < $custom_date) {
              $build[$field]['#access'] = FALSE;
            }
            break;

          case 'custom_great':
            if (strtotime($info['value']) > $custom_date) {
              $build[$field]['#access'] = FALSE;
            }
            break;

          case 'older_than':
            // Comming soon...
            $build[$field]['#access'] = TRUE;
            break;

          case 'newer_than':
            // Comming soon...
            $build[$field]['#access'] = TRUE;
            break;

          default:
            $build[$field]['#access'] = FALSE;
        }
      }
    }

    if (empty($build[$field]['#items'])) {
      $build[$field]['#access'] = FALSE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function summary($settings) {
    $time_span = isset($settings['settings']['time_span']) ? $settings['settings']['time_span'] : NULL;
    $orientations = [
      'small' => t("smaller than today's date"),
      'great' => t("greater than today's date"),
      'custom_small' => t("smaller then custom date"),
      'custom_great' => t("greater then custom date"),
      'older_than' => t("older than"),
      'newer_than' => t("newer than"),
    ];

    switch ($settings['settings']['orientation']) {
      case ('custom_small'):
        $display_date = ' - ' . $settings['settings']['cutom_date'];
        break;

      case ('custom_great'):
        $display_date = ' - ' . $settings['settings']['cutom_date'];
        break;

      case ('older_than'):
        $display_date = ' ' . $time_span . ' days';
        break;

      case ('newer_than'):
        $display_date = ' ' . $time_span . '  days';
        break;

      default:
        $display_date = '';
    }

    return t('Condition: %condition (%orientation%date)', [
      "%condition" => t('Hide date/time'),
      '%orientation' => $orientations[$settings['settings']['orientation']],
      '%date' => $display_date,
    ]);

  }

}
